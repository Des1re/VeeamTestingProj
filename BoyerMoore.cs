﻿using System;
using System.Collections.Generic;

static class BoyerMoore
{
    public static List<long> Search(byte[] str, byte[] pat, long addOffset = 0)
    {
        List<long> values = new List<long>();

        int m = pat.Length;
        int n = str.Length;

        int[] badChar = new int[256];

        BadCharHeuristic(pat, m, ref badChar);

        int s = 0;
        while (s <= (n - m))
        {
            int j = m - 1;

            while (j >= 0 && pat[j] == str[s + j])
                --j;

            if (j < 0)
            {
                values.Add(s + addOffset);
                s += (s + m < n) ? (m - badChar[str[s + m]]) : 1;
            }
            else
            {
                s += Math.Max(1, j - badChar[str[s + j]]);
            }
        }
        return values;
    }

    private static void BadCharHeuristic(byte[] str, int size, ref int[] badChar)
    {
        int i;

        for (i = 0; i < 256; i++)
            badChar[i] = -1;

        for (i = 0; i < size; i++)
            badChar[(int)str[i]] = i;
    }
}