﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Threading;
using Microsoft.VisualBasic.Devices;

namespace VeeamTest
{
    class Program
    {
        public struct GZipParametrs
        {
            public ThreadPriority ThreadPriority { get; set; }
            public long BlocksSize { get; set; }
            public int BlocksMax { get; set; }
        }

        static string HelpString
        {
            get
            {
                return $@"Please use command:
{System.AppDomain.CurrentDomain.FriendlyName} compress/decompress <input_file> <output_file (optional)>

Available parametrs:
    -p 0-4
    --Priority 0-4
        Set threads priority
        0 - Lowest
        1 - BelowNormal
        2 - Normal (by default)
        3 - AboveNormal
        4 - Highest


    -bs <KB_size>
    --BlockSize <KB_size>
        Only for compress: size of compressed block;
        1024 by default.

    -bm <int>
    --BlockMax <int>
        Setting the maximum number of blocks in memory (to save RAM);
        by default 0: unlimited;
        recommended to set more than the number of threads in the CPU;
        use if your file is too large for free RAM or you see a lot of OutOfMemoryException errors.";
            }
        }

        static int Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine(HelpString);
                return 1;
            }

            string action = args[0].ToLower();
            string inputFile = args[1];

            string outputFile = null;
            if (args.Length >= 3)
                outputFile = args[2];

            bool IsSuccessfully = false;


            try
            {
                if (!File.Exists(inputFile))
                {
                    Console.WriteLine($"File \"{inputFile}\" does not exist");
                    return 1;
                }

                GZipParametrs gzipParametrs = ReadParametrs(args);

                using (GZip gZip = new GZip(blocksMax: gzipParametrs.BlocksMax, threadsPriority: gzipParametrs.ThreadPriority))
                {
                    gZip.Logger += GZip_Logger;
                    switch (action)
                    {
                        case "compress":
                            IsSuccessfully = gZip.Compress(inputFile, outputFile, blocksSize: gzipParametrs.BlocksSize);
                            break;
                        case "decompress":
                            IsSuccessfully = gZip.Decompress(inputFile, outputFile);
                            break;
                        default:
                            IsSuccessfully = false;
                            Console.WriteLine($"Commands \"{action}\" does not exist");
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error: {exception.Message}");
                IsSuccessfully = false;
            }

            if (IsSuccessfully)
            {
                Console.WriteLine($"{action} successfully completed!");
                return 0;
            }
            else
            {
                Console.WriteLine($"{action} failed. Please check console log");
                return 1;
            }
        }

        private static void GZip_Logger(string message)
        {
            Console.WriteLine(message);
        }

        static GZipParametrs ReadParametrs(string[] args)
        {
            GZipParametrs gzipParametrs = new GZipParametrs()
            {
                BlocksSize = 1024 * 1024,
                ThreadPriority = ThreadPriority.Normal
            };
            Dictionary<string, string> paramsFromArgs = new Dictionary<string, string>();

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].StartsWith("-"))
                {
                    if (i + 1 < args.Length && !args[i + 1].StartsWith("-"))
                        paramsFromArgs.Add(args[i], args[++i]);
                    else
                        paramsFromArgs.Add(args[i], null);
                }
            }

            foreach (var param in paramsFromArgs)
            {
                switch (param.Key)
                {
                    case "-p":
                    case "--Priority":
                        ThreadPriority tp;
                        int numericalPriority;
                        if (Int32.TryParse(param.Value, out numericalPriority) && numericalPriority <= 4 && numericalPriority >= 0)
                        {
                            Enum.TryParse(numericalPriority.ToString(), out tp);
                            gzipParametrs.ThreadPriority = tp;
                        }
                        break;
                    case "-bs":
                    case "--BlockSize":
                        long bs = 0;
                        if (Int64.TryParse(param.Value, out bs) && bs > 0)
                            gzipParametrs.BlocksSize = bs * 1024;
                        break;
                    case "-bm":
                    case "--BlockMax":
                        int bm = 0;
                        if (Int32.TryParse(param.Value, out bm) && bm > 0)
                            gzipParametrs.BlocksMax = bm;
                        break;
                }
            }
            return gzipParametrs;
        }
    }
}
