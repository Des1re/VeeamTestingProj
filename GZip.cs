﻿using System;
using System.IO;
using System.IO.Compression;
using System.IO.MemoryMappedFiles;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace VeeamTest
{
    class GZip : IDisposable
    {
        #region Compress fields
        private long compress_BlockSize;
        #endregion

        #region Decompress fields
        private readonly static byte[] decompress_GZippedPrefix = new byte[] { 31, 139, 8, 0, 0, 0, 0, 0, 4, 0 };
        List<long> decompress_GZippedBlocks = new List<long>();
        #endregion

        #region General fields
        private string _FilePath;
        private string FilePath
        {
            get { return _FilePath; }
            set
            {
                _FilePath = value;
                MemMappedFile = MemoryMappedFile.CreateFromFile(FilePath, FileMode.Open);
                OriginalFileInfo = new FileInfo(value);
            }
        }

        private readonly int GzipThreadCount;
        private readonly int MaxActiveBlocksInMemory;
        private readonly ThreadPriority ThreadsPriority;

        private static long CurrentOffset = 0;
        private static bool IsFileEnded { get; set; }

        private static ManualResetEvent MREventOutMemory = new ManualResetEvent(false);

        private readonly object lockOriginalBlocksPool = new object();
        private Dictionary<long, MemoryMappedViewStream> OriginalBlocks = new Dictionary<long, MemoryMappedViewStream>();

        private readonly object lockPreparedBlocksPool = new object();
        private Dictionary<long, MemoryStream> BlocksPreparedToWriteInFile = new Dictionary<long, MemoryStream>();

        private List<Thread> ThreadsPool = new List<Thread>();

        private FileInfo OriginalFileInfo { get; set; }
        private MemoryMappedFile MemMappedFile { get; set; }
        private FileStream OutputFile { get; set; }
        #endregion

        public delegate void LogHandler(string message);
        public event LogHandler Logger;


        public GZip(int blocksMax = 0, ThreadPriority threadsPriority = ThreadPriority.Normal)
        {
            GzipThreadCount = (Environment.ProcessorCount > 2) ? Environment.ProcessorCount - 2 : Environment.ProcessorCount;
            MaxActiveBlocksInMemory = blocksMax;
            ThreadsPriority = threadsPriority;
        }

        public void Dispose()
        {
            foreach (var thread in ThreadsPool)
                thread.Abort();

            if (MemMappedFile != null)
                MemMappedFile.Dispose();
            if (OutputFile != null)
                OutputFile.Dispose();
            if (MREventOutMemory != null)
                MREventOutMemory.Dispose();

            decompress_GZippedBlocks = null;

            foreach(var stream in OriginalBlocks)
                stream.Value.Dispose();

            foreach (var stream in BlocksPreparedToWriteInFile)
                stream.Value.Dispose();

            OriginalBlocks = null;
            BlocksPreparedToWriteInFile = null;
        }

        #region Compress
        public bool Compress(string filePath, string outputPath, long blocksSize = 1024 * 1024)
        {
            try
            {
                FilePath = filePath;
                if (outputPath == null)
                    OutputFile = new FileStream(FilePath + ".gz", FileMode.Create, FileAccess.Write);
                else
                    OutputFile = new FileStream(outputPath.EndsWith(".gz") ? outputPath : (outputPath + ".gz"), FileMode.Create, FileAccess.Write);

                compress_BlockSize = blocksSize;

                OriginalFileInfo = new FileInfo(FilePath);

                Thread threadReadFile = new Thread(ReadFileBlocks)
                {
                    Name = "ReadFile",
                    Priority = ThreadsPriority
                };
                ThreadsPool.Add(threadReadFile);
                threadReadFile.Start();

                for (int i = 0; i < GzipThreadCount; i++)
                {
                    Thread threadGZip = new Thread(CompressThread)
                    {
                        Name = i.ToString(),
                        Priority = ThreadsPriority
                    };
                    ThreadsPool.Add(threadGZip);
                    threadGZip.Start();
                    Logger?.Invoke($"Thread #{i} started");
                }

                Thread threadWriteFile = new Thread(new ParameterizedThreadStart(WriteOutputFile))
                {
                    Name = "ReadFile",
                    Priority = ThreadsPriority
                };
                ThreadsPool.Add(threadWriteFile);
                threadWriteFile.Start(true);
                threadWriteFile.Join();

                Logger?.Invoke($"File compressed to {OutputFile.Name}");
                return true;
            }
            catch (Exception exception)
            {
                Logger?.Invoke($"Error: {exception.Message}");
                return false;
            }
        }

        private void CompressThread()
        {
            while (!IsFileEnded || OriginalBlocks.Count > 0)
            {

                long offset;

                MemoryMappedViewStream inputStream;
                lock (lockOriginalBlocksPool)
                {
                    if (OriginalBlocks.Count > 0)
                    {
                        var block = OriginalBlocks.First();
                        offset = block.Key;
                        inputStream = block.Value;
                        OriginalBlocks.Remove(offset);
                    }
                    else
                        continue;
                }

                bool letsTryAgain;
                do
                {
                    letsTryAgain = false;

                    MemoryStream outputStream = new MemoryStream();
                    try
                    {
                        using (GZipStream compressionStream = new GZipStream(outputStream, CompressionMode.Compress, true))
                        {
                            inputStream.CopyTo(compressionStream);
                            inputStream.Dispose();
                        }

                        lock (lockPreparedBlocksPool)
                            BlocksPreparedToWriteInFile.Add(offset, outputStream);

                        Logger?.Invoke($"Block compressed (offset: {offset})");
                    }
                    catch (Exception exception)
                    {
                        if (exception is OutOfMemoryException || exception is IOException)
                        {
                            Logger?.Invoke($"Error: {exception.Message}");
                            outputStream.Dispose();
                            letsTryAgain = true;
                            MREventOutMemory.WaitOne();
                        }
                        else
                            throw exception;
                    }
                    finally
                    {
                        inputStream.Dispose();
                    }
                } while (letsTryAgain);
            }
        }

        private void ReadFileBlocks()
        {
            long offset = 0;
            while (offset < OriginalFileInfo.Length)
            {

                long allocateMemory;
                if (offset + compress_BlockSize > OriginalFileInfo.Length)
                    allocateMemory = OriginalFileInfo.Length - offset;
                else
                    allocateMemory = compress_BlockSize;

                bool letsTryAgain;
                do
                {
                    letsTryAgain = false;
                    try
                    {
                        if (MaxActiveBlocksInMemory == 0 || OriginalBlocks.Count < MaxActiveBlocksInMemory)
                        {
                            MemoryMappedViewStream inputStream = MemMappedFile.CreateViewStream(offset, allocateMemory, MemoryMappedFileAccess.Read);
                            lock (lockOriginalBlocksPool)
                                OriginalBlocks.Add(offset, inputStream);
                            Logger?.Invoke($"File block readed (offset: {offset})");
                        }
                        else
                            letsTryAgain = true;
                    }
                    catch (Exception exception)
                    {
                        if (exception is OutOfMemoryException || exception is IOException)
                        {
                            Logger?.Invoke($"Error: {exception.Message}");
                            letsTryAgain = true;
                            MREventOutMemory.WaitOne();
                        }
                        else
                            throw exception;
                    }
                } while (letsTryAgain);

                offset += compress_BlockSize;
            }
            IsFileEnded = true;

            CompressThread(); // switch thread to compressing
        }
        #endregion

        #region Decompress
        public bool Decompress(string filePath, string outputPath)
        {
            try
            {
                FilePath = filePath;
                if (outputPath == null)
                    OutputFile = new FileStream(FilePath.Remove(OriginalFileInfo.FullName.Length - OriginalFileInfo.Extension.Length), FileMode.Create, FileAccess.Write);
                else
                    OutputFile = new FileStream(outputPath, FileMode.Create, FileAccess.Write);

                Thread threadReadFile = new Thread(DecompressIOThread)
                {
                    Name = "ReadFile",
                    Priority = ThreadsPriority
                };
                ThreadsPool.Add(threadReadFile);
                threadReadFile.Start();

                for (int i = 0; i < GzipThreadCount; i++)
                {
                    Thread threadGZip = new Thread(DecompressThread)
                    {
                        Name = i.ToString(),
                        Priority = ThreadsPriority

                    };
                    ThreadsPool.Add(threadGZip);
                    threadGZip.Start();
                }

                Thread threadWriteFile = new Thread(new ParameterizedThreadStart(WriteOutputFile))
                {
                    Name = "ReadFile",
                    Priority = ThreadsPriority
                };
                ThreadsPool.Add(threadWriteFile);
                threadWriteFile.Start(false);
                threadWriteFile.Join();

                Logger?.Invoke($"File decompressed to {OutputFile.Name}");
                return true;
            }
            catch (Exception exception)
            {
                Logger?.Invoke($"Error: {exception.Message}");
                return false;
            }
        }

        private void DecompressThread()
        {
            try
            {
                while (!IsFileEnded || OriginalBlocks.Count > 0)
                {

                    long offset;
                    MemoryMappedViewStream compressedBlock;
                    lock (lockOriginalBlocksPool)
                    {
                        if (OriginalBlocks.ContainsKey(CurrentOffset))
                        {
                            offset = CurrentOffset++;
                            compressedBlock = OriginalBlocks[offset];
                            OriginalBlocks.Remove(offset);
                        }
                        else
                            continue;
                    }


                    bool letsTryAgain;
                    do
                    {
                        letsTryAgain = false;

                        if (MaxActiveBlocksInMemory != 0)
                        {
                            if (BlocksPreparedToWriteInFile.Count > MaxActiveBlocksInMemory)
                            {
                                letsTryAgain = true;
                                continue;
                            }
                        }

                            MemoryStream decompressedStream = new MemoryStream();
                            try
                            {
                                using (GZipStream decompressionStream = new GZipStream(compressedBlock, CompressionMode.Decompress, true))
                                {
                                    decompressionStream.CopyTo(decompressedStream);
                                }
                                lock (lockPreparedBlocksPool)
                                    BlocksPreparedToWriteInFile.Add(offset, decompressedStream);

                                Logger?.Invoke($"Block decompressed (offset: {offset})");
                            }
                            catch (Exception exception)
                            {
                                if (exception is OutOfMemoryException || exception is IOException || exception is InvalidDataException)
                                {
                                    Logger?.Invoke($"Error: {exception.Message}");
                                    decompressedStream.Dispose();
                                    letsTryAgain = true;
                                    MREventOutMemory.WaitOne();
                                }
                                else
                                    throw exception;
                            }
                        
                    } while (letsTryAgain);
                }
            }
            catch (Exception exception)
            {
                Logger?.Invoke($"Error while trying to decompress the file: {exception.Message}");
                Environment.Exit(1);
            }

        }
        
        private void DecompressIOThread()
        {
            SearchCompressedBlocks();
            ReadCompressedBlocks();
            IsFileEnded = true;

            DecompressThread(); // switch thread to decompressing
        }

        private void SearchCompressedBlocks()
        {
            try
            {
                long searchBlockSize = 1024 * 1024;
                long offset = 0;
                while (offset < OriginalFileInfo.Length)
                {
                    long allocateMemory;
                    if (offset + searchBlockSize > OriginalFileInfo.Length)
                        allocateMemory = OriginalFileInfo.Length - offset;
                    else
                        allocateMemory = searchBlockSize;

                    byte[] currentBlock = new byte[allocateMemory];

                    using (MemoryMappedViewStream inputStream = MemMappedFile.CreateViewStream(offset, allocateMemory, MemoryMappedFileAccess.Read))
                    {
                        inputStream.Read(currentBlock, 0, Convert.ToInt32(allocateMemory));
                        List<long> gzippedBlocks = BoyerMoore.Search(currentBlock, decompress_GZippedPrefix, offset);
                        if (gzippedBlocks.Count > 0)
                            decompress_GZippedBlocks.AddRange(gzippedBlocks);
                    }

                    offset += searchBlockSize - decompress_GZippedPrefix.Length + 1;
                }
            }
            catch (Exception exception)
            {
                Logger?.Invoke($"Error while searching the compressed blocks: {exception.Message}");
                Environment.Exit(1);
            }
        }
        
        private void ReadCompressedBlocks()
        {
            try
            {
                int offset = 0;
                while (offset < decompress_GZippedBlocks.Count)
                {
                    long allocateMemory = ((offset == decompress_GZippedBlocks.Count - 1) ? (OriginalFileInfo.Length) : (decompress_GZippedBlocks[offset + 1])) - decompress_GZippedBlocks[offset];

                    bool letsTryAgain;
                    do
                    {
                        letsTryAgain = false;
                        try
                        {
                            if (MaxActiveBlocksInMemory == 0 || OriginalBlocks.Count < MaxActiveBlocksInMemory)
                            {
                                MemoryMappedViewStream inputStream = MemMappedFile.CreateViewStream(decompress_GZippedBlocks[offset], allocateMemory, MemoryMappedFileAccess.Read);
                                lock (lockOriginalBlocksPool)
                                    OriginalBlocks.Add(offset, inputStream);
                                Logger?.Invoke($"Comressed block readed (offset: {offset})");
                            }
                            else
                                letsTryAgain = true;
                        }
                        catch (Exception exception)
                        {
                            if (exception is OutOfMemoryException || exception is IOException)
                            {
                                Logger?.Invoke($"Error: {exception.Message}");
                                letsTryAgain = true;
                                MREventOutMemory.WaitOne();
                            }
                            else
                                throw exception;
                        }
                    } while (letsTryAgain);

                    offset++;
                }
            }
            catch (Exception exception)
            {
                Logger?.Invoke($"Error while trying to read the compressed file: {exception.Message}");
                Environment.Exit(1);
            }
        }
        #endregion

        private void WriteOutputFile(object forCompressParametr)
        {
            bool forCompress = (bool)forCompressParametr;
            int i = 0;

            long writeOffset;
            long nextOffset;

            while (true)
            {
                if (forCompress)
                {
                    writeOffset = compress_BlockSize * i;
                    nextOffset = compress_BlockSize * (i + 1);
                    if (writeOffset >= OriginalFileInfo.Length)
                        break;
                }
                else
                {
                    writeOffset = i;
                    nextOffset = i + 1;
                    if (writeOffset >= decompress_GZippedBlocks.Count && IsFileEnded)
                        break;
                }

                if (BlocksPreparedToWriteInFile.ContainsKey(writeOffset))
                {
                    BlocksPreparedToWriteInFile[writeOffset].WriteTo(OutputFile);
                    BlocksPreparedToWriteInFile[writeOffset].Dispose();
                    lock (lockPreparedBlocksPool)
                        BlocksPreparedToWriteInFile.Remove(writeOffset);
                    i++;
                    MREventOutMemory.Set();

                    Logger?.Invoke($"Block writed to the file (offset: {writeOffset})");
                }
            }
        }
    }
}
